package com.example.demo.rest;

import com.example.demo.dto.MovieDto;
import com.example.demo.service.impl.MovieServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class MovieApiController {

    @Autowired
    private MovieServiceImpl movieService;

    @CrossOrigin
    @GetMapping("/movies")
    public ResponseEntity getMovies(){
        return ResponseEntity.ok().body(movieService.getMovieRepository());
    }
}
