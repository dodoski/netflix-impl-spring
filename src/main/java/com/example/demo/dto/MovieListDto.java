package com.example.demo.dto;

import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;


public class MovieListDto {
    private List<MovieDto> movies;

    public List<MovieDto> getMovies() {
        return movies;
    }

    public MovieListDto(List<MovieDto> movies) {
        this.movies = movies;
    }
}
