package com.example.demo.service.impl;

import com.example.demo.dto.MovieDto;
import com.example.demo.dto.MovieListDto;
import com.example.demo.service.MovieRepository;
import com.example.demo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;


@Service
public class MovieServiceImpl implements MovieService {

   @Autowired
    private MovieRepository movieRepository;

    public MovieListDto getMovieRepository() {
        return movieRepository.getMoviesList();
    }
}
