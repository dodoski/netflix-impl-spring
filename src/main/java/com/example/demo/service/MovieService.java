package com.example.demo.service;

import com.example.demo.dto.MovieDto;
import com.example.demo.dto.MovieListDto;

import java.util.List;

public interface MovieService {
    public MovieListDto getMovieRepository();
}
